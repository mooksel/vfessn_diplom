## Build frontend

```
cd vfessn/static/vfessn
npm install
npm build
```

## Build docker image
```
docker build -t vfessn .
```

## Run container
```
docker run --rm --name vfessn -p 8080:8080 vfessn
```
