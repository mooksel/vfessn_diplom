import sys,os
sys.path.append(os.getcwd())


from flask import Flask, jsonify, request
from vfessn import db


app = Flask(__name__, static_folder='static/')


@app.route('/')
def hello_world():
    return app.send_static_file('vfessn/build/index.html')


@app.route('/events', methods=['GET'])
def get_events():
    events = db.select_all_events()

    return jsonify([{
        'id': event['id'],
        'description': event['description'],
        'imageUrl': f'/image/{event["image_name"]}',
    } for event in events])


@app.route('/book', methods=['POST'])
def book():
    payload = request.json

    db.save_booking_record(
        phone=payload['phone'],
        name=payload['name'],
        event_id=payload['eventId'],
    )

    return ''


@app.route('/static/<path:path>')
def send_js(path):
    return app.send_static_file(f'vfessn/build/static/{path}')


@app.route('/image/<path:path>')
def send_medis(path):
    return app.send_static_file(f'images/{path}')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
