export function getEvents() {
    return fetch('/events').then((response) => {
        return response.json();
    });
}


export function book(payload) {
    console.log(payload);
    return fetch('/book', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    })
}
