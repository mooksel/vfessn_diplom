import React from 'react';
import './style.css';

export default function SessionRow(props) {
    return (
        <div
            className="SessionRow-container"
            onClick={() => props.onClick(props.id)}
        >
            <img
                src={props.imageUrl}
                className="SessionRow-image"
            />
            <div className="SessionRow-description">
                <h2>{props.description}</h2>
            </div>
        </div>
    )
}
