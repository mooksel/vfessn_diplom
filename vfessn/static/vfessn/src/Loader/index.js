import React from 'react';
import './style.css';

export default function Loader(props) {
    return (
        <div className="Loader-wrap">
            <div className="lds-roller">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    )
}
