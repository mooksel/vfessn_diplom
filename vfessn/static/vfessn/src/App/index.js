import React from 'react';
import './style.css';

import EventRow from '../EventRow';
import Loader from '../Loader';
import BookingForm from '../BookingForm';

import * as Api from '../api';


const AppStatus = {
  LOADING: 0,
  EVENTS: 1,
  BOOKING_FORM: 2,
}


export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      events: [],
      bookingEvent: null,
      status: AppStatus.LOADING,
    }
  }

  componentDidMount() {
    setTimeout(() => {
      Api.getEvents().then((events) => {
        this.setState({
          ...this.state,
          events: events,
          status: AppStatus.EVENTS,
        });
      });
    }, 1000);
  }

  book(data) {
    this.setState({
      ...this.state,
      status: AppStatus.LOADING,
    });

    console.log(data);


    setTimeout(() => {
      Api.book(data).then(() => {
        this.setState({
          ...this.state,
          status: AppStatus.EVENTS,
        });
      });
    }, 1000);
  }

  openBookingForm(eventId) {
    this.setState({
      ...this.state,
      status: AppStatus.LOADING,
    });

    const event = this.state.events.filter((event) => event.id == eventId)[0];

    setTimeout(() => {
      this.setState({
        ...this.state,
        status: AppStatus.BOOKING_FORM,
        bookingEvent: event,
      });
    }, 1000.0);
  }

  renderEvents() {
    return (
      <div className="App-sessionsWrap">
        {this.state.events.map((event) => {
          return (
            <EventRow
              key={event.id}
              id={event.id}
              imageUrl={event.imageUrl}
              description={event.description}
              onClick={(eventId) => this.openBookingForm(eventId)}
            />
          )
        })}
      </div>
    );
  }

  renderLoading() {
    return (
      <Loader />
    );
  }

  renderBookingForm() {
    return (
      <BookingForm event={this.state.bookingEvent} onSubmit={(data) => {
        this.book(data);
      }} />
    );
  }

  renderContent() {
    const status = this.state.status;

    if (status === AppStatus.BOOKING_FORM) {
      return this.renderBookingForm();
    } else if (status === AppStatus.EVENTS) {
      return this.renderEvents();
    }

    return this.renderLoading();
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>Advent Theatre</h1>
        </div>
        <div className="App-content">
          {this.renderContent()}
        </div>
        <div className="App-footer">
          <div>
            <h5>Developed by ВЄРОНІКА</h5>
          </div>
        </div>
      </div>
    );
  }

}
