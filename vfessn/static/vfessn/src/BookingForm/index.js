import React from 'react';
import './style.css';


export default class BookingForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            phone: '',
            name: '',
        }
    }

    onNameChange(e) {
        this.setState({
            ...this.state,
            name: e.target.value,
        });
    }

    onPhoneChange(e) {
        this.setState({
            ...this.state,
            phone: e.target.value,
        });
    }

    render() {
        const event = this.props.event;

        return (
            <div className="BookingForm-wrap">
                <div className="BookingForm-form">
                    <h1 className="BookingForm-description">
                        {event.description}
                    </h1>
                    <table>
                        <tbody>
                            <tr>
                                <td className="BookingForm-label">
                                    <b>Name:</b>
                                </td>
                                <td>
                                    <input
                                        className="BookingForm-input"
                                        type="text"
                                        onChange={(e) => this.onNameChange(e)}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="BookingForm-label">
                                    <b>Phone:</b>
                                </td>
                                <td>
                                    <input
                                        className="BookingForm-input"
                                        type="text"
                                        onChange={(e) => this.onPhoneChange(e)}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input
                        className="BookingForm-submitButton"
                        type="submit"
                        value="Submit"
                        onClick={() => {
                            this.props.onSubmit({
                                name: this.state.name,
                                phone: this.state.phone,
                                eventId: this.props.event.id,
                            })
                        }}
                    />
                </div>
            </div>
        );
    }

}
