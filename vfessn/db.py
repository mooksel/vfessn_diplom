import sqlite3


__DB_FILE = 'theatre.db'


def init():
    db_connection = sqlite3.connect(__DB_FILE)
    db_connection.execute(
        '''
            CREATE TABLE IF NOT EXISTS event (
                id INT PRIMARY KEY,
                description VARCHAR(128) NOT NULL,
                image_name VARCHAR(128) NOT NULL
            );
        '''
    )

    db_connection.execute(
        '''
            CREATE TABLE IF NOT EXISTS booking_record (
                id INT PRIMARY KEY,
                event_id INT NOT NULL,
                name VARCHAR(64) NOT NULL,
                phone VARCHAR(20) NOT NULL
            );
        '''
    )

def select_all_events():
    db_connection = sqlite3.connect(__DB_FILE)

    cur = db_connection.cursor()
    cur.execute('SELECT * FROM event;')

    rows = cur.fetchall()

    return [{
        'id': row[0],
        'description': row[1],
        'image_name': row[2],
    } for row in rows]


def save_booking_record(name, phone, event_id):
    db_connection = sqlite3.connect(__DB_FILE)
    db_connection.execute(
        f'''
            INSERT INTO booking_record (
                event_id,
                phone,
                name
            ) VALUES (?, ?, ?);
        '''
    , (event_id, phone, name))
    db_connection.commit()
