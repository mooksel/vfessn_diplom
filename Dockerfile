FROM python:3.7-alpine

WORKDIR /work

COPY vfessn/__init__.py  vfessn/__init__.py
COPY vfessn/db.py  vfessn/db.py
COPY vfessn/main.py  vfessn/main.py
COPY theatre.db  theatre.db
COPY requirements.txt requirements.txt
COPY vfessn/static/images vfessn/static/images
COPY vfessn/static/vfessn/build vfessn/static/vfessn/build


RUN pip install --no-deps -r requirements.txt

EXPOSE 8080

CMD [ "python", "vfessn/main.py" ]
